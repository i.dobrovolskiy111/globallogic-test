Add-WindowsFeature Web-Scripting-Tools
Import-Module WebAdministration
$iisAppPoolName = "my-test-app"
$iisAppPoolDotNetVersion = "v4.0"
$iisAppName = "my-test-app.test"
$iisWebsiteName = "my-website"
$iissitefolderName = "my-website-folder"
$directoryPath = "C:\Users\redline\test"

cd IIS:\AppPools\


if (!(Test-Path $iisAppPoolName -pathType container))
{
    
    $appPool = New-Item $iisAppPoolName
    $appPool | Set-ItemProperty -Name "managedRuntimeVersion" -Value $iisAppPoolDotNetVersion
}


cd IIS:\Sites\


if (Test-Path $iisAppName -pathType container)
{
    return
}


$iisApp = New-Item $iisAppName -bindings @{protocol="http";bindingInformation=":80:" + $iisAppName} -physicalPath $directoryPath
$iisApp | Set-ItemProperty -Name "applicationPool" -Value $iisAppPoolName
$iisApp | Set-ItemProperty -Name "website" -Value $iisWebsiteName
$iisApp | Set-ItemProperty -Name "siteFolder" -Value $iissitefolderName