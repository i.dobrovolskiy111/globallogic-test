variable "Zone" {
  default = ["1", "2"]
}
variable "PrivateIP" {
  default = ["10.0.1.5", "10.0.1.6"]
}
