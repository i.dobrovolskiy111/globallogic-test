# Configure the Microsoft Azure Provider
provider "azurerm" {
  version = "~>2.0"
  features {}
}

# Create a resource group if it doesn't exist
resource "azurerm_resource_group" "myterraformgroup" {
  name     = "myResourceGroup"
  location = "eastus"
  tags = {
    environment = "Terraform Demo"
  }
}
#LoadBalancer
resource "azurerm_lb" "task_1" {
  name                = "LoadBalancer"
  location            = "eastus"
  resource_group_name = azurerm_resource_group.myterraformgroup.name
  #sku                 = "standard"
}
# Create virtual network
resource "azurerm_virtual_network" "myterraformnetwork" {
  name                = "myVnet"
  address_space       = ["10.0.0.0/16"]
  location            = "eastus"
  resource_group_name = azurerm_resource_group.myterraformgroup.name
  tags = {
    environment = "Terraform Demo"
  }
}

# Create subnet
resource "azurerm_subnet" "myterraformsubnet" {
  name                 = "mySubnet"
  resource_group_name  = azurerm_resource_group.myterraformgroup.name
  virtual_network_name = azurerm_virtual_network.myterraformnetwork.name
  address_prefixes     = ["10.0.1.0/24"]
}

# Create public IPs
#resource "azurerm_public_ip" "myterraformpublicip" {
#name                = "myPublicIP"
#location            = "eastus"
#resource_group_name = azurerm_resource_group.myterraformgroup.name
#allocation_method   = "Static"
#ku                 = "standard"
#zones               = ["1"]
#availability_zone = 1
resource "azurerm_public_ip" "myterraformpublicip" {
  count               = 2
  name                = "test01-pip-${count.index}"
  sku                 = "Standard"
  availability_zone   = var.Zone[count.index]
  resource_group_name = azurerm_resource_group.myterraformgroup.name
  location            = "eastus"
  allocation_method   = "Static"
}

#tags = {
#  environment = "Terraform Demo"
#}
#
#resource "azurerm_network_interface" "myterraformnic" {
#name                = ["myNIC", "myNIC1"]
#  location            = "eastus"
#  resource_group_name = azurerm_resource_group.myterraformgroup.name



#  ip_configuration {
#    name                          = "myNicConfiguration"
#    private_ip_address_allocation = "dynamic"
#    public_ip_address_id          = azurerm_public_ip.myterraformpublicip.id
#  }
#}

resource "azurerm_network_interface" "myterraformnic" {
  count               = 2
  name                = "vm-${count.index}-nic"
  resource_group_name = azurerm_resource_group.myterraformgroup.name
  location            = "eastus"
  dns_servers         = var.PrivateIP

  ip_configuration {
    name                          = "myNicConfiguration"
    subnet_id                     = azurerm_subnet.myterraformsubnet.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.PrivateIP[count.index]
    public_ip_address_id          = azurerm_public_ip.myterraformpublicip[count.index].id
  }
}

## Create 2 Windows Virtual Machine
resource "azurerm_windows_virtual_machine" "virtual_machine" {
  count               = 2
  name                = "AZDC-${count.index}"
  resource_group_name = azurerm_resource_group.myterraformgroup.name
  location            = "eastus"
  size                = "Standard_DS1_v2"
  admin_username      = "redline"
  admin_password      = "Password@1234"
  zone                = var.Zone[count.index]

  network_interface_ids = [
    azurerm_network_interface.myterraformnic[count.index].id
  ]

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "MicrosoftWindowsServer"
    offer     = "WindowsServer"
    sku       = "2019-Datacenter"
    version   = "latest"
  }
  depends_on = [
    azurerm_network_interface.myterraformnic
  ]
}
